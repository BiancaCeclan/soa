package edu.ubb.soa.requests;

public class LyricsRequest {
    private String artistName;
    private String title;

    private LyricsRequest() {
    }

    public LyricsRequest(String firstName, String lastName) {
        this.artistName = firstName;
        this.title = lastName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
