package edu.ubb.soa.service;

import edu.ubb.soa.requests.LyricsRequest;
import edu.ubb.soa.transfer.objects.Lyrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
public class LyricsService {

    @Autowired
    private RestTemplate restTemplate;

    private static final String LYRICS_PATH = "http://localhost:8081/lyrics";

    public List<String> retrieveLyrics(final String artist, final String title) {
        HttpEntity<Object> requestEntity = createRequestEntity(artist, title);
        try {
            ResponseEntity<Lyrics> response = restTemplate.exchange(LYRICS_PATH, HttpMethod.POST, requestEntity,
                    new ParameterizedTypeReference<Lyrics>() {
                    });
            return convertToLocalModel(response.getBody());
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    private List<String> convertToLocalModel(Lyrics lyricsTransferObject) {
        return Optional.ofNullable(lyricsTransferObject)
                .map(Lyrics::getLyrics)
                .map(lyrics -> lyrics.split("\n"))
                .map(Arrays::asList)
                .orElse(Collections.emptyList());
    }

    private HttpEntity<Object> createRequestEntity(String artist, String title) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(new LyricsRequest(artist, title), headers);
    }

}
