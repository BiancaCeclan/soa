package edu.ubb.soa.service;

import edu.ubb.soa.model.Song;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicLong;

@Component
public class SongService {

    private static AtomicLong counter = new AtomicLong();

    private final ConcurrentMap<Long, Song> songs = new ConcurrentHashMap<>();

    public Iterable<Song> findAll() {
        return songs.values();
    }

    public Song save(Song song) {
        Long id = song.getId();
        if (id == null) {
            id = counter.incrementAndGet();
            song.setId(id);
        }
        songs.put(id, song);
        return song;
    }

    public void deleteSong(Long id) {
        songs.remove(id);
    }
}
