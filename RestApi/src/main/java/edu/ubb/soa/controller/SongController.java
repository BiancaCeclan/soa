package edu.ubb.soa.controller;

import edu.ubb.soa.model.Song;
import edu.ubb.soa.service.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SongController {

    @Autowired
    private SongService songService;

    @GetMapping("/songs")
    public Iterable<Song> getSongs() {
        return songService.findAll();
    }

    @PostMapping("/songs/delete")
    public Iterable<Song> delete(@RequestBody Long id) {
        songService.deleteSong(id);
        return songService.findAll();
    }

    @PostMapping(name = "/songs/create")
    public Song create(@RequestBody Song song) {
        return songService.save(song);
    }

}
