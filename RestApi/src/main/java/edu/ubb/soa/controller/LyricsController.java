package edu.ubb.soa.controller;

import edu.ubb.soa.requests.LyricsRequest;
import edu.ubb.soa.service.LyricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class LyricsController {

    @Autowired
    private LyricsService lyricsService;

    @PostMapping(path = "/lyrics", consumes = "application/json")
    public List<String> lyrics(@RequestBody final LyricsRequest request) {
        return lyricsService.retrieveLyrics(request.getArtistName(), request.getTitle());
    }

}
