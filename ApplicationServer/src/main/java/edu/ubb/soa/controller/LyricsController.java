package edu.ubb.soa.controller;

import edu.ubb.soa.delegate.LyricsDelegate;
import edu.ubb.soa.model.Lyrics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LyricsController {

    @Autowired
    private LyricsDelegate lyricsDelegate;

    public Lyrics getLyrics(final String artist, final String title) {
        return lyricsDelegate.retrieveLyrics(artist, title);
    }

}
