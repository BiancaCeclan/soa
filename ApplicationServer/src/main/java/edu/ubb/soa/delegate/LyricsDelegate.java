package edu.ubb.soa.delegate;

import com.google.gson.Gson;
import edu.ubb.soa.model.Lyrics;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class LyricsDelegate {

    private static final String AUTHORS_ENDPOINT = "https://api.lyrics.ovh/v1/%s/%s";
    private static final String ENCODED_SPACE = "%20";

    public Lyrics retrieveLyrics(final String artist, final String title) {
        try {
            HttpURLConnection con = createConnection(String.format(AUTHORS_ENDPOINT, encodeString(artist), encodeString(title)));
            String theString = IOUtils.toString(con.getInputStream(), "UTF-8");
            return new Gson().fromJson(theString, Lyrics.class);
        } catch (Exception e) {
            e.printStackTrace();
            return new Lyrics();
        }
    }

    private String encodeString(final String value) {
        return value.replace(" ", ENCODED_SPACE);
    }

    private HttpURLConnection createConnection(final String urlAddress) throws IOException {
        URL url = new URL(urlAddress);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Accept", "application/json");
        return con;
    }

}
