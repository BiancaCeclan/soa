package edu.ubb.soa;

import edu.ubb.soa.controller.LyricsController;
import edu.ubb.soa.model.Lyrics;
import edu.ubb.soa.requests.LyricsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

    @Autowired
    private LyricsController lyricsController;

    @PostMapping(path = "/lyrics", consumes = "application/json")
    public Lyrics lyrics(@RequestBody LyricsRequest request) {
        return lyricsController.getLyrics(request.getArtistName(), request.getTitle());
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
