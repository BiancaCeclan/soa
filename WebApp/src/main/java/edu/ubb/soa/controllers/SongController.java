package edu.ubb.soa.controllers;

import edu.ubb.soa.domain.Song;
import edu.ubb.soa.services.SongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SongController {

    private SongService songService;

    @Autowired
    public void setSongService(SongService songService) {
        this.songService = songService;
    }

    @RequestMapping(value = "/songs", method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("songs", songService.listAllSongs());
        return "songs";
    }

    @RequestMapping("song/{id}")
    public String showSong(@PathVariable Integer id, Model model){
        model.addAttribute("song", songService.getSongById(id));
        return "songshow";
    }

    @RequestMapping("song/edit/{id}")
    public String edit(@PathVariable Integer id, Model model){
        model.addAttribute("song", songService.getSongById(id));
        return "songform";
    }

    @RequestMapping("song/new")
    public String newSong(Model model){
        model.addAttribute("song", new Song());
        return "songform";
    }

    @RequestMapping(value = "song", method = RequestMethod.POST)
    public String saveSong(Song song){
        Song savedSong = songService.saveSong(song);
        return "redirect:/song/" + savedSong.getId();
    }

    @RequestMapping("song/delete/{id}")
    public String delete(@PathVariable Integer id){
        songService.deleteSong(id);
        return "redirect:/songs";
    }

}
