package edu.ubb.soa.services;

import edu.ubb.soa.domain.Song;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class SongService {

    private static final String RETRIEVE_ENDPOINT = "http://localhost:8082/songs";
    private static final String SAVE_ENDPOINT = "http://localhost:8082/songs/create";
    private static final String DELETE_ENDPOINT = "http://localhost:8082/songs/delete";

    private static final String RETRIEVE_LYRICS_ENDPOINT = "http://localhost:8082/lyrics";

    @Autowired
    private RestTemplate restTemplate;

    public List<Song> listAllSongs() {
        HttpEntity<Object> requestEntity = createRequestEntity();
        ResponseEntity<List<Song>> response = restTemplate.exchange(RETRIEVE_ENDPOINT, HttpMethod.GET, requestEntity,
                new ParameterizedTypeReference<List<Song>>() {
                });
        return response.getBody();
    }

    public Song getSongById(Integer id) {
        Song song = listAllSongs()
                .stream()
                .filter(s -> s.getId().equals(id))
                .findFirst()
                .orElse(new Song());
        if (song.getArtistName() != null && song.getTitle() != null) {
            song.setLyrics(getLyrics(song));
        }
        return song;
    }

    public Song saveSong(Song song) {
        HttpEntity<Object> requestEntity = createRequestEntity(song);
        ResponseEntity<Song> response = restTemplate.exchange(SAVE_ENDPOINT, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<Song>() {
                });
        return response.getBody();
    }

    public void deleteSong(Integer id) {
        HttpEntity<Object> requestEntity = createRequestEntity(id);
        restTemplate.exchange(DELETE_ENDPOINT, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<List<Song>>() {
                });
    }

    private List<String> getLyrics(Song song) {
        HttpEntity<Object> requestEntity = createRequestEntity(song);
        ResponseEntity<List<String>> response = restTemplate.exchange(RETRIEVE_LYRICS_ENDPOINT, HttpMethod.POST, requestEntity,
                new ParameterizedTypeReference<List<String>>() {
                });
        return response.getBody();
    }

    private HttpEntity<Object> createRequestEntity(final Integer id) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(id, headers);
    }

    private HttpEntity<Object> createRequestEntity(final Song song) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(song, headers);
    }

    private HttpEntity<Object> createRequestEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(null, headers);
    }

}
